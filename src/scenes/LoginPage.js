import React , {Component} from 'react'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import styled from 'styled-components'
import Icon from '@material-ui/core/Icon';
import {TextField,Button} from '@material-ui/core';

const Layout = styled.div`
    width: 100%
    height: 100%
    display: flex
    flex-direction: column
    overflow: scroll
`

const BackGround = styled.div`
    width: 100%;
    height: 100%
    position: fixed;
    top: 0;
    left: 0;
    z-index: -10;
    background-size: cover;
    background-image: url(${require('../assets/images/hompage-bg.jpg')});
`
const Head = styled.section`
    display: flex
    flex-direction: column
    justify-content: center
    align-items: center
    height: 15vh;
    width: 100%
`

const Content = styled.div`
    width: 100%;
    height: 60vh;
    display: flex;
    justify-content: center;
    align-items: center;
`

const Card = styled.div`
    width: 300px;
    height: 360px;
    box-shadow: 0 6px 20px 0 rgba(0, 0, 0, 0.19), 0 8px 17px 0 rgba(0, 0, 0, 0.2);
    background-color: #ffffff;
    margin: 0 10px 0 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 30px
`

const LoginForm = styled.form`
    width: 90%;
    display: flex;
    flex-direction: column;
`

const LoginFacebookBtn = styled(Button)`
    && {
        background: linear-gradient(#4C69BA 30%, #4C69BA 100%);
        width: 90%;
        color: white;
        &:hover {
            filter: brightness(0.9)
        }
    }
`

const LoginBtn = styled(Button)`
    && {
        background: linear-gradient(#4ABDAC 30%, #4ABDAC 100%);
        width: 90%;
        color: white;
        &:hover {
            filter: brightness(0.9)
        }
    }
`

const FacebookLogo = styled.div`
    width: 20px
    height: 20px
    margin: 0 5px 0 0 ;
    background-image: url(${require('../assets/images/facebook-logo.png')});
    background-size: cover;
`


export default class LoginPage extends Component{
    state = {
        email:'',
        password:''
    }
    render(){
        return(
            <Layout>
                <BackGround/>
                <Navbar/>
                <Head>
                    <h1 style={{color:'white',fontSize:'40px'}}>เข้าสู่ระบบ</h1>
                </Head>
                <Content>
                    <Card>
                        <Icon style={{fontSize:'100px'}}>face</Icon>
                        <LoginForm>
                            <TextField
                                style={{width:'100%'}}
                                required
                                type='email'
                                value={this.state.email}
                                label="Email"
                                onChange={(e)=>{this.setState({email:e.target.value})}}
                            />
                            <div style={{height:'20px'}}/>
                            <TextField
                                style={{width:'100%'}}
                                required
                                type='password'
                                value={this.state.password}
                                label="Password"
                                onChange={(e)=>{this.setState({password:e.target.value})}}
                            />
                        <div style={{height:'30px'}}/>
                        </LoginForm>
                        <LoginFacebookBtn>
                            <FacebookLogo/>
                            เข้าสู่ระบบผ่าน Facebook
                        </LoginFacebookBtn>
                        <div style={{height:'20px'}}/>
                        <LoginBtn>
                            เข้าสู่ระบบ
                        </LoginBtn>
                    </Card>
                </Content>
                <div style={{height:'50px'}}/>
                <Footer/>
            </Layout>
        )
    }
}